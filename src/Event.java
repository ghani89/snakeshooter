import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;



/**
 * @author 	BOUZIANE Abdelghani
 * Date : 22/09/13
 *
 */
public class Event implements KeyListener , MouseListener , MouseMotionListener , MouseWheelListener{
	
	public  int left,right,up,down,jump,attack,enter,erase,pause;
	public  int add , remove , curX , curY , save , next , previous ;
	

	@Override
	public void keyPressed(KeyEvent e) {
		
		switch(e.getKeyCode())
		{
		case KeyEvent.VK_ESCAPE | KeyEvent.VK_CANCEL:
			System.exit(0);
			break;
		case KeyEvent.VK_DELETE:
			erase=1;
			break;
		case KeyEvent.VK_C:
			jump=1;
			break;
		
		case KeyEvent.VK_V:
			attack=1;
			break;
		case KeyEvent.VK_LEFT:
			left=1;
			break;
		case KeyEvent.VK_RIGHT:
			right=1;
			break;
		case KeyEvent.VK_DOWN:
			down=1;
			break;
		case KeyEvent.VK_UP:
			up=1;
			break;
		case KeyEvent.VK_ENTER:
			enter=1;
			break;
		case KeyEvent.VK_P:
			pause=1;
			break;
		case KeyEvent.VK_S:
			save = 1 ;
			break;
		}
		
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
		switch(e.getKeyCode())
		{
		case KeyEvent.VK_DELETE:
			erase=0;
			break;
		case KeyEvent.VK_C:
			jump=0;
			break;
		
		case KeyEvent.VK_V:
			attack=0;
			break;
		case KeyEvent.VK_LEFT:
			left=0;
			break;
		case KeyEvent.VK_RIGHT:
			right=0;
			break;
		case KeyEvent.VK_DOWN:
			down=0;
			break;
		case KeyEvent.VK_UP:
			up=0;
			break;
		case KeyEvent.VK_ENTER:
			enter=0;
			break;
		case KeyEvent.VK_P:
			pause=0;
			break;
		case KeyEvent.VK_S:
			save = 0 ;
			break;
		}
		
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		switch ( e.getButton() )
		{
			case MouseEvent.BUTTON1 :
				add = 1 ;
				break;
			case MouseEvent.BUTTON3 :
				remove = 1 ;
				break;
				
		}
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
		switch ( e.getButton() )
		{
			case MouseEvent.BUTTON1 :
				add = 0 ;
				break;
			case MouseEvent.BUTTON3 :
				remove = 0;
				break;
				
		}

	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		
		curX = e.getX() ; 
		curY = e.getY() ;
		
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		
		
		if(e.getWheelRotation()<0) //up 
			next=1;
		else
			if(e.getWheelRotation()>0) // down
				previous=1;
		
	}
	
	

}
