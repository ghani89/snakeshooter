import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.util.ArrayList;


/**
 * @author 	BOUZIANE Abdelghani
 * Date : 22/09/13
 *
 */

public class Snake implements Global {
	
	private static final int VITESSE = 10;
	
	
	//Direction de  snake 
	final int UP    = 0 ;
	final int DOWN  = 1 ;
	final int LEFT  = 2 ; 
	final int RIGHT = 3 ;
	
	protected final int iQueue = 4 ;
	ArrayList<Rectangle> body ; 
    protected int vitesse  ;
    protected Event event;
    protected int direction ; 
    protected int MAX_X , MAX_Y;
    protected Image[] snakeSprite ;
    
	
	public Snake(Event event)
	{
		
		this.event = event ;
		MAX_X = SCREEN_WIDTH /ELEMENT_WIDTH ;
		MAX_Y = SCREEN_HEIGHT / ELEMENT_HEIGHT ;
		snakeSprite = new Image[]
		                        { Biblio.makeColorTransparent(Toolkit.getDefaultToolkit().getImage("image/t_up.png"),Color.WHITE),
				 				  Biblio.makeColorTransparent(Toolkit.getDefaultToolkit().getImage("image/t_down.png"),Color.WHITE), 
						          Biblio.makeColorTransparent(Toolkit.getDefaultToolkit().getImage("image/t_left.png"),Color.WHITE),
								  Biblio.makeColorTransparent(Toolkit.getDefaultToolkit().getImage("image/t_right.png"),Color.WHITE),
								  Biblio.makeColorTransparent(Toolkit.getDefaultToolkit().getImage("image/queue.png"),Color.WHITE)
								};
		initSnake();
	}
	
	public void initSnake()
	{
		
		vitesse = VITESSE ;
		direction = RIGHT ;
		body = new ArrayList<Rectangle>();
		this.addElement( MAX_X / 2 , MAX_Y / 2);
		
	}
	public void update()
	{
		getDirection();
		move();
		
	}
	
	public void draw(Graphics buffer)
	{
		
		buffer.drawImage(snakeSprite[direction],body.get(0).x * ELEMENT_WIDTH, body.get(0).y * ELEMENT_HEIGHT, null);
		if(body.size() > 1)
		{			
			for(int i = 1 ; i < body.size();i++)
				buffer.drawImage(snakeSprite[iQueue],body.get(i).x * ELEMENT_WIDTH, body.get(i).y * ELEMENT_HEIGHT, null);
		}
		
	}
	
	protected void addElement(int x , int y)
	{
		Rectangle rect = new Rectangle(x,y,ELEMENT_WIDTH , ELEMENT_HEIGHT );
		body.add(rect);
	}
	
	private void getDirection()
	{
		if( event.up == 1 && direction != DOWN ) 
			direction = UP ;
		else
		   if( event.down == 1 && direction != UP ) 
			direction = DOWN ;
		else 
		   if ( event.left == 1 && direction != RIGHT)
			   direction = LEFT ;
		else
		   if(event.right == 1 && direction != LEFT)
			   direction = RIGHT ; 
		
	}
	
	private void move()
	{
		if(vitesse <=0)
		{
			vitesse = VITESSE ;
				
				Rectangle rect = new Rectangle(body.get(0));
				body.remove(body.size()-1);
				switch ( direction )
				{
					case UP :
						rect.y -- ;
						break;
					case DOWN : 
						rect.y ++ ;
						break;	
					case LEFT : 
						rect.x -- ;
						break;
					case RIGHT : 
						rect.x ++ ;
						break;
				}
				
				if ( rect.x > MAX_X - 1 ) rect.x = 0 ;
				if ( rect.x < 0 ) 	  rect.x = MAX_X - 1 ;
				if ( rect.y > MAX_Y - 1) rect.y = 0 ;
				if ( rect.y < 0 )	  rect.y = MAX_Y - 1;
				
				body.add(0, rect);
			
		}
		else
		{
			vitesse -- ;
		}
	}
	public boolean selfCollisionTest()
	{
		
		
		for ( int i = 1 ; i < body.size() ; i++ )
		{
			
			if( (body.get(0).x ==  body.get(i).x) && (body.get(0).y ==  body.get(i).y) )
			{
				return true ;
			}
		}
		
		return false ;
	}
	
	public void mapCollision( Map map )
	{
		switch ( map.getElemAt(body.get(0).x, body.get(0).y ) )
		 {
		 	case MUR :
		 		initSnake();
		 		break;
		 	case NURRITURE :
		 		addElement(-1, -1);
		 		map.setMap(body.get(0).x, body.get(0).y, VIDE);
		 		break;
		 }		
	}
}
