

import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.io.File;
import java.io.IOException;

import javax.imageio.*;


public class Biblio implements Global{

	public static long getTicks()
	{
		return System.currentTimeMillis();
	}
	public static void delay(long frameLimit )
	{
		/* Gestion des 60 fps (images/stories/seconde) */
		long ticks=getTicks();
		if(frameLimit<ticks)
			return;
		if(frameLimit>ticks+FPS_WAIT)
			try {
				Thread.sleep(FPS_WAIT);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			else
			{
				try {
					Thread.sleep(frameLimit-ticks);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
			}
	}
	
	public static Image makeColorTransparent(Image im,final Color color)
	{
		ImageFilter filter=new RGBImageFilter() {
			public int makerRGB=color.getRGB()|0xFF000000;
			@Override
			public int filterRGB(int x, int y, int rgb) {
				if((rgb|0xFF000000)==makerRGB)
				{
					return 0x00FFFFFF & rgb;
				}else
					return rgb;
			}
		};
		
		ImageProducer ip=new FilteredImageSource(im.getSource(), filter);
		return Toolkit.getDefaultToolkit().createImage(ip);
	}
	
	public static void sleep (long delay) 
	{
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static BufferedImage loadImage(String image)
	{
		BufferedImage img=null;
		try {
			img = ImageIO.read(new File(image));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return img;
	}
}
