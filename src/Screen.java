import java.awt.Cursor;
import java.awt.DisplayMode;
import java.awt.Graphics;
import java.awt.GraphicsDevice;
import java.awt.Point;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;

/**
 * @author BOUZIANE Abdelghani
 * date : 19/09/13
 */
public class Screen extends JFrame {
	
	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;
	protected BufferStrategy strategy;
	protected Graphics buffer;
	
	public Screen( int width , int height )
	{	
		initGraphics(width,height);
		setVisible(true);
	}
	
	protected void initGraphics( int width , int height )
	{
		setUndecorated(true);
		GraphicsDevice myDevice=java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		myDevice.setFullScreenWindow(this);
		myDevice.setDisplayMode(new DisplayMode(width, height, 32,DisplayMode.REFRESH_RATE_UNKNOWN));
		setIgnoreRepaint( true );
		createBufferStrategy(2);
		strategy = getBufferStrategy();
	    buffer = strategy.getDrawGraphics();
	}

	public void hideCursor()
	{
		setCursor(getToolkit().createCustomCursor(
	              new BufferedImage(3, 3, BufferedImage.TYPE_INT_ARGB),
	              new Point(0, 0), "null"));
	}
	
	public void showCursor()
	{
		setCursor(Cursor.DEFAULT_CURSOR);
		
	}

	
	
	public void clearScreen()
	{
		buffer.clearRect(0, 0,getWidth(), getHeight());
	}
	
	public void showScreen()
	{
		strategy.show();
	}
	public Graphics getBuffer()
	{
		return buffer;
	}
}
