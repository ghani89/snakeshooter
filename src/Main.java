


public class Main implements Global {

	/**
	 * 
	 * @param args
	 */
	

	
	public static void main(String[] args) {
		
		
		Screen ecran = new Screen( SCREEN_WIDTH,SCREEN_HEIGHT);
		Event event = new Event();
		Snake snake = new Snake(event);
		Map map = new Map();
		Editor editor = new Editor(event , map );
		Intro intro = new Intro(ecran.getBuffer());
		Menu menu = new Menu(ecran.getBuffer(), event);
		ecran.addKeyListener(event);
		ecran.addMouseListener(event);
		ecran.addMouseMotionListener(event);
		ecran.addMouseWheelListener(event);
		ecran.hideCursor();
		long frameLimit=Biblio.getTicks()+FPS_WAIT;
		
		boolean start  = false ;
		
		while(true)
		{
			
			ecran.clearScreen();
			if ( event.enter == 1 ) 
				start = true;
			if (event.pause == 1)
				start = false ; 
			
			if(start)
			{
				// game started
				//menu.update();
				//menu.draw();
			
			snake.update();
		    snake.mapCollision( map );
			
			if (snake.selfCollisionTest())
				snake.initSnake();
			if(map.nbrNurriture() <=0 )
			{
				map.nextLevel();
				snake.initSnake();
			}
			
			editor.update();
			map.draw(ecran.getBuffer());
			snake.draw(ecran.getBuffer());
			editor.draw(ecran.getBuffer());
			}
			else
			{
				// intro started 
				intro.update();
				intro.draw();
			}
			ecran.showScreen();
			Biblio.delay(frameLimit);
			frameLimit=Biblio.getTicks()+FPS_WAIT;
		}
		 	
	}

}
