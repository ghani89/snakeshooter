
/**
 * @author 	BOUZIANE Abdelghani
 * Date : 22/09/13
 *
 */

public interface Global {
	
	static final int SCREEN_WIDTH= 640;
	static final int SCREEN_HEIGHT= 480;
	
	final int ELEMENT_WIDTH  = 20 ; 
	final int ELEMENT_HEIGHT = 20 ;
	
	static final int MAX_MAP_X = SCREEN_WIDTH / ELEMENT_WIDTH ;
	static final int MAX_MAP_Y = SCREEN_HEIGHT / ELEMENT_HEIGHT ;
	static final String LEVEL_FILE = "Levels/level";
	
	static final int MUR = 1 ;
	static final int VIDE = 0 ;
	static final int NURRITURE = 2 ;
	
	static final int FPS = 60 ;
	static final int FPS_WAIT = 1000 / FPS ;

}
