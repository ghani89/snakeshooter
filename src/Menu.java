import java.awt.Color;
import java.awt.Graphics;


public class Menu implements Global {
	
	protected Graphics buffer ;
	protected Event event ;
	protected Label jouer ;
	protected Label Editeur ; 
	protected Label	quitter ;
	
	public Menu(Graphics buffer , Event event)
	{
		this.buffer = buffer ;
		this.event = event ;
		creerLabels();
	}
	
	protected void creerLabels()
	{
		jouer = new Label("JOUER", SCREEN_WIDTH / 3 , SCREEN_HEIGHT /6 , Color.WHITE, Color.DARK_GRAY, Color.GRAY);
		Editeur = new Label("EDITEUR", SCREEN_WIDTH / 3 , SCREEN_HEIGHT /3 , Color.WHITE, Color.DARK_GRAY, Color.GRAY);
		quitter = new Label("QUITTER", SCREEN_WIDTH / 3 , SCREEN_HEIGHT /2 , Color.WHITE, Color.DARK_GRAY, Color.GRAY);
		
		jouer.setBuffer(buffer);
		jouer.setEventListner(event);
		
		Editeur.setBuffer(buffer);
		Editeur.setEventListner(event);
		
		quitter.setBuffer(buffer);
		quitter.setEventListner(event);
	}
	
	public void update()
	{
		if(quitter.isPressed())
			System.exit(0);
	}
	
	public void draw()
	{
		buffer.setColor(Color.ORANGE);
		buffer.fillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
		jouer.draw();
		Editeur.draw();
		quitter.draw();
	}

}
