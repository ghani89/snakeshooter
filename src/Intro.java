import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;


/*
 * BOUZIANE ABDELGHANI 
 * DATE : 27/09/13
 */

public class Intro implements Global{
	
	protected final String[] introText = { 
										  "Ghani Games" ,
										  "Présente" , 
										  "Snake Shooter" ,
										  "Programmé par" ,
										  "BOUZIANE Abdelghani"										  
									    } ;	
	protected int textIndex ;
	protected final int VITESSE_TEXT = 4 ;
	protected final int vx = 10 ;
	protected int tpos_x , tpos_y , vitesse ; 
	protected Graphics buffer ; 
	 
	public Intro(Graphics buffer)
	{
		this.buffer = buffer ;
		init();
	}
	
	public void init()
	{
		buffer.setFont(new Font("DigifaceWide", Font.BOLD + Font.ITALIC, 28));
		textIndex = 0 ;
		tpos_x  = 0 ;
		tpos_y = SCREEN_HEIGHT / 2 - buffer.getFontMetrics().getHeight() / 2 ;
		vitesse = VITESSE_TEXT ;
	}
	
	public void update()
	{
		if(vitesse > 0)
		{
			vitesse -- ; 
		}else
		{
			vitesse = VITESSE_TEXT ;
			tpos_x += vx ;
			if ( tpos_x > (SCREEN_WIDTH / 2 -  buffer.getFontMetrics().stringWidth( introText[textIndex]) / 2 ))
			{
				tpos_x = 0 ;
				textIndex ++ ;
				if( textIndex >= introText.length )
					textIndex = 0 ;
			}
		}
		
	}
	
	public void draw()
	{
		buffer.setColor(Color.BLACK);
		buffer.fillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
		buffer.setColor(Color.WHITE);
		buffer.drawString(introText[textIndex],tpos_x, tpos_y);
	}
	
}
