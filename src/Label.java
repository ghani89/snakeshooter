import java.awt.Color;
import java.awt.Graphics;


public class Label {
	
	protected String text ; 
	protected int x , y ;
	protected Color textColor ;	
    protected Color bgColor ; 
    protected Color fColor ; //focus color 
    protected Event event ;
    protected final int MARGE = 5 ;
    protected Graphics buffer ;
    
    public Label(String text, int x, int y, Color textColor, Color bgColor , Color fColor)
	{
		super();
		this.text = text;
		this.x = x;
		this.y = y;
		this.textColor = textColor;
		this.bgColor = bgColor;
		this.fColor = fColor;
	}
    
    
    public boolean isPressed()
    {
    	
    	int  w , h , x1 , y1 ;
    	w = buffer.getFontMetrics().stringWidth(text);
    	h = buffer.getFontMetrics().getHeight();
    	x1 = x - MARGE ;
    	y1 = y - MARGE ; 
    	w =  w + 2*MARGE ;
    	h =  h + 2*MARGE ;
    	
    	if ( event.add == 1 && ( event.curX >= x1 && event.curX <= x1 + w ) && ( event.curY >= y1 && event.curY <= y1 + h ) )
    		return true ;
    	else
    		return false ;
    	
    	
    }
    
    public void draw()
    {
    	int  w , h , x1 , y1 ;
    	w = buffer.getFontMetrics().stringWidth(text);
    	h = buffer.getFontMetrics().getHeight();
    	x1 = x - MARGE ;
    	y1 = y - MARGE ; 
    	w =  w + 2*MARGE ;
    	h =  h + 2*MARGE ;
    	
    	if( ( event.curX >= x1 && event.curX <= x1 + w ) && ( event.curY >= y1 && event.curY <= y1 + h ) )
    		buffer.setColor(fColor);
    	else
    		buffer.setColor(bgColor);

    	buffer.fillRect(x1 , y1 , w , h );
    	buffer.setColor(textColor);
    	buffer.drawString( text , x , y + h-(2*MARGE));
    	
    }
    
    
    public void setEventListner(Event event)
    {
    	this.event = event ;
    }
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Color getTextColor() {
		return textColor;
	}

	public void setTextColor(Color textColor) {
		this.textColor = textColor;
	}

	public Color getBgColor() {
		return bgColor;
	}

	public void setBgColor(Color bgColor) {
		this.bgColor = bgColor;
	}

	public Color getfColor() {
		return fColor;
	}

	public void setfColor(Color fColor) {
		this.fColor = fColor;
	}

	public void setBuffer(Graphics buffer)
	{
		this.buffer = buffer ;
	}
    
	
	

}
