import java.awt.Color;
import java.awt.Graphics;

/**
 * @author 	BOUZIANE Abdelghani
 * Date : 25/09/13
 *
 */

public class Editor implements Global {
	
	
	protected Event event ; 
	protected Map map; 
	protected int typeTile ; 
	public Editor(Event event , Map map)
	{
		this.event = event ; 
		this.map = map ;
		typeTile = MUR ; 
	}
	
	public void update()
	{
		
		if ( event.add == 1 ) 
			map.setMap(event.curX / ELEMENT_WIDTH, event.curY / ELEMENT_HEIGHT , typeTile ) ; 
		else
			if (event.remove == 1 )
				map.setMap(event.curX / ELEMENT_WIDTH, event.curY / ELEMENT_HEIGHT , VIDE ) ;
		if( event.save == 1 )
		{
			map.saveMap();
			event.save = 0 ;
		}
		
		if(event.next == 1 )
		{
			typeTile ++ ; 
			if(typeTile > NURRITURE) 
				typeTile = VIDE ;
			event.next = 0;
		}
		
		if( event.previous == 1 )
		{
			typeTile -- ;
			if( typeTile < VIDE )
				typeTile = NURRITURE ; 
			event.previous = 0 ;
		}
		
	}

	
	public void draw(Graphics buffer)
	{
		switch ( typeTile )
		{
					case MUR :
						buffer.setColor(Color.BLACK);
						buffer.fillRect( event.curX , event.curY , ELEMENT_WIDTH , ELEMENT_HEIGHT );
						break ;
					case NURRITURE :
						buffer.setColor(Color.blue);
						buffer.fillRect( event.curX , event.curY , ELEMENT_WIDTH , ELEMENT_HEIGHT );
						break ;
		}
	}
	
}
