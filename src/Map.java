import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;


/**
 * @author 	BOUZIANE Abdelghani
 * Date : 22/09/13
 *
 */

public class Map implements Global{
	
	 
	protected int maxX , maxY ;
	protected int startX , startY ;
	protected int[][] map;
	protected int level ;
	protected long nbrLevels;
	protected Image nurriture , arriere_plan , mur ;
	
	public Map()
	{
		this.maxX = SCREEN_WIDTH /ELEMENT_WIDTH ;
		this.maxY = SCREEN_HEIGHT / ELEMENT_HEIGHT ;
		startX = startY = 0 ;
		level = 1 ;
		nurriture = Biblio.makeColorTransparent(Toolkit.getDefaultToolkit().getImage("image/queue.png"),Color.WHITE);
		arriere_plan = Toolkit.getDefaultToolkit().getImage("image/plan.png");
		mur = Toolkit.getDefaultToolkit().getImage("image/mur.png");
		//calcule de nombre de niveaux existant 
		nbrLevels = (new File("Levels")).list().length;
		//chargement de premier niveau
		loadMap(LEVEL_FILE+level+".txt");
	}
	
	protected void loadMap(String fileName ) {
		
		Scanner file=null;
		
		try {
			file = new Scanner(new File(fileName));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if( file == null )
		{
			System.out.println("Failed to open map "+fileName);
			System.exit(1);
		}
		
		map=new int[MAX_MAP_Y][MAX_MAP_X];
						
		for(int y=0;y<MAX_MAP_Y;y++)
		{
			for(int x=0;x<MAX_MAP_X;x++)
			{
				map[y][x]=file.nextInt();
											
			}
		}
		
		startX=startY=0;
		
		file.close();
	}
	
	protected void saveMap() {
		PrintWriter fr=null;
		try {
			//cr�ation d'un nouveau niveau 
			fr=new PrintWriter(new FileWriter(LEVEL_FILE+(nbrLevels+1)+".txt"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
			if(fr==null )
			{
				System.out.println("Failed to open map "+LEVEL_FILE+(nbrLevels+1)+".txt");
				System.exit(1);
			}
			
			for(int y=0;y<MAX_MAP_Y;y++)
			{
				for(int x=0;x<MAX_MAP_X;x++)
				{
					fr.append(""+map[y][x]);			
					fr.append(" ");
				}
				fr.append("\n");
			}
			
			
			fr.close();
			//le nombre de niveau augmente 
			nbrLevels ++ ;
		
		
		
	}

	protected void reinitMap(String fileName) {
		
		
		PrintWriter fr=null;
		try {
			fr=new PrintWriter(new FileWriter(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
			if(fr==null )
			{
				System.out.println("Failed to open map "+fileName);
				System.exit(1);
			}
			
			for(int y=0;y<MAX_MAP_Y;y++)
			{
				for(int x=0;x<MAX_MAP_X;x++)
				{
					
					fr.print(0);
					fr.print(" ");
				}
				fr.println();
			}
			
			
			fr.close();
		

		
	}

	
	public void draw(Graphics buffer)
	{
		buffer.setColor(Color.orange);
		buffer.fillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
		//buffer.drawImage(arriere_plan, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, null);
		for(int y = startY ; y < startY + maxY ; y++ )
		{
			for(int x = startX ; x < startX + maxX ; x++ )
			{	switch ( map[y][x] )
				{
					case MUR :
						//buffer.setColor(Color.BLACK);
						//buffer.fillRect( ( x - startX ) * ELEMENT_WIDTH , ( y - startY ) * ELEMENT_HEIGHT , ELEMENT_WIDTH , ELEMENT_HEIGHT );
						buffer.drawImage(mur , ( x - startX ) * ELEMENT_WIDTH , ( y - startY ) * ELEMENT_HEIGHT ,null );
						break ;
					case NURRITURE :						
						buffer.drawImage(nurriture , ( x - startX ) * ELEMENT_WIDTH , ( y - startY ) * ELEMENT_HEIGHT ,null );
						break ;
				}

			}
		}

	}
	
	public int nbrNurriture()
	{
		int nb = 0 ;
		for(int y=0;y<MAX_MAP_Y;y++)
		{
			for(int x=0;x<MAX_MAP_X;x++)
			{
				if(map[y][x] == NURRITURE) 
					nb++;
			}
		}
		return nb ;
	}
	public void setMap( int x , int y , int val )
	{
		map[y][x] = val ;
	}
	
	public int getElemAt( int x , int y )
	{
		return map[y][x];
	}
	
	public void nextLevel()
	{
		if( level >= nbrLevels ) 
			level = 1 ;
		else
			level ++ ;
		
		loadMap(LEVEL_FILE+level+".txt");
	}

}
